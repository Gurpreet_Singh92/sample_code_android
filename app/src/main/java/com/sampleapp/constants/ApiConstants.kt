package com.sampleapp.constants


object ApiConstants {

    const val BASE_URL: String = "https://simplifiedcoding.net"
    const val HEROES_LIST: String = "/demos/marvel/"

}