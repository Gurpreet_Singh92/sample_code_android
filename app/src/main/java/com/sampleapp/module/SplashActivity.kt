package com.sampleapp.module

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity

import com.sampleapp.R
import com.sampleapp.module.heroesList.HeroesListActivity
/*import rx.Observable
import rx.Subscription*/

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            startActivity(Intent(this, HeroesListActivity::class.java))
            finish()
        }, 2000)

    }
}
