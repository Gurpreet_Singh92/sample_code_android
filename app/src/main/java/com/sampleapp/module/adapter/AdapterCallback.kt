package com.sampleapp.module.adapter

interface AdapterCallback {
    fun onItemClicked(name: String,realName: String, team: String, firstappearance: String, publisher: String, imageurl: String, bio: String)
}