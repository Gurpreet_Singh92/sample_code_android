package com.sampleapp.module.adapter
import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sampleapp.R
import com.sampleapp.data.response.HeroesListResponse

import kotlinx.android.synthetic.main.list_items.view.*


class ItemsListAdapter(val items: HeroesListResponse, val context: Context, var adapteritemclick: AdapterCallback) : RecyclerView.Adapter<ViewHolder>() {

    // Gets the number of items in the list
    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_items, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.tvName?.text = items.get(position).name


        holder.card_view?.setOnClickListener(View.OnClickListener {
            adapteritemclick.onItemClicked(items.get(position).name,items.get(position).realname,items.get(position).team,items.get(position).firstappearance,items.get(position).publisher,items.get(position).imageurl,items.get(position).bio)

        })
    }
}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    val tvName = view.tv_name
    val card_view=view.card_view

}