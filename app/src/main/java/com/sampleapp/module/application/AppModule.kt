package com.sampleapp.module.application

import android.content.Context
import com.sampleapp.utils.AppUtils


import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class AppModule(context: Context) {

    private val mContext: Context = context


    @Provides
    @Singleton
    fun getAPpUtils(): AppUtils {
        return AppUtils(mContext)
    }
}