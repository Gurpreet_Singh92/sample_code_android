package com.sampleapp.module.application

import android.app.Application
import android.content.Context
import com.sampleapp.R
import com.sampleapp.module.heroesList.HeroesListActivity
import com.sampleapp.module.heroesList.HeroesListComponent
import com.sampleapp.module.heroesList.HeroesListModule
import com.sampleapp.module.heroesDetail.HeroesDetailActivity
import com.sampleapp.module.heroesDetail.HeroesDetailComponent
import com.sampleapp.module.heroesDetail.HeroesDetailModule
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump
import io.github.inflationx.viewpump.ViewPumpContextWrapper


class SampleApplication : Application() {

    private var heroesDetailComponent: HeroesDetailComponent? = null
    private var heroesListComponent: HeroesListComponent? = null

    override fun onCreate() {
        super.onCreate()

        ViewPump.init(ViewPump.builder()
                .addInterceptor(CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/roboto_regular.ttf")
                                .setFontAttrId(R.attr.fontPath)
                                .build())).build())

    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase))
    }

    //create app component
    private fun createAppComponent(): AppComponent {
        return DaggerAppComponent.builder().appModule(AppModule(this)).build()
    }

    //create login component
    fun createHeroDetailComponent(heroesDetailActivity: HeroesDetailActivity): HeroesDetailComponent? {
        heroesDetailComponent = createAppComponent().plus(HeroesDetailModule(heroesDetailActivity))
        return heroesDetailComponent
    }

    fun createHeroesListComponent(heroesListActivity: HeroesListActivity): HeroesListComponent? {
        heroesListComponent = createAppComponent().plus(HeroesListModule(heroesListActivity))
        return heroesListComponent
    }


    //release created login component
    fun releaseComponent() {
        heroesListComponent = null
    }
}