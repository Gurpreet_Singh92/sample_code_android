package com.sampleapp.module.application

import com.sampleapp.api.NetModule
import com.sampleapp.module.heroesList.HeroesListComponent
import com.sampleapp.module.heroesList.HeroesListModule
import com.sampleapp.module.heroesDetail.HeroesDetailComponent
import com.sampleapp.module.heroesDetail.HeroesDetailModule
import dagger.Component
import javax.inject.Singleton



@Singleton
@Component(modules = [(AppModule::class), (NetModule::class)])
interface AppComponent {

    fun plus(loginModule: HeroesDetailModule): HeroesDetailComponent
    fun plus(heroesListModule: HeroesListModule): HeroesListComponent
}