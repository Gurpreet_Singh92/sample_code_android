package com.sampleapp.module.heroesList

import dagger.Subcomponent


@HeroesScope
@Subcomponent(modules = [(HeroesListModule::class)])
interface HeroesListComponent {
    fun inject(heroesListActivity: HeroesListActivity)
}