package com.sampleapp.module.heroesList

import javax.inject.Scope


@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class HeroesScope