package com.sampleapp.module.heroesList

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.sampleapp.R
import com.sampleapp.data.response.HeroesListResponse
import com.sampleapp.module.adapter.AdapterCallback
import com.sampleapp.module.adapter.ItemsListAdapter
import com.sampleapp.module.application.SampleApplication
import com.sampleapp.module.heroesDetail.HeroesDetailActivity
import com.sampleapp.mvp.BaseMvpActivity
import com.sampleapp.utils.AppUtils
import com.sampleapp.utils.ProgressBarHandler
import kotlinx.android.synthetic.main.activity_heroes_list.*
import javax.inject.Inject


class HeroesListActivity: BaseMvpActivity(), HeroesListContract.View, AdapterCallback {

    @Inject
    lateinit var mPresenter: HeroesListPresenter
    @Inject
    lateinit var mProgressBarHandler: ProgressBarHandler


    @Inject
    lateinit var appUtils: AppUtils

    lateinit var  adapteritemclick:AdapterCallback

    lateinit var mLayoutManager: LinearLayoutManager

    override fun heroesList(heroesListResponse: HeroesListResponse) {
        setUI(heroesListResponse)
    }

    private fun setUI(heroesListResponse: HeroesListResponse) {
        rv_list.layoutManager = mLayoutManager
        rv_list.adapter = ItemsListAdapter(heroesListResponse, this, adapteritemclick)
    }

    override fun showProgress() {
        mProgressBarHandler.showProgress()
    }

    override fun hideProgress() {
        mProgressBarHandler.hideProgress()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_heroes_list)
         (application as SampleApplication).createHeroesListComponent(this)?.inject(this)
        mPresenter.attachView(this)
        mLayoutManager = LinearLayoutManager(this)
        adapteritemclick=this
        setSupportActionBar(myToolbar);
        if (appUtils.verifyAvailableNetwork(this) == true) {
            mPresenter.fetchMarvelHeroesList()
        } else {
            Toast.makeText(this, resources.getString(R.string.warning_network_error), Toast.LENGTH_LONG).show()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean { // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean { // Handle action bar item clicks here. The action bar will
// automatically handle clicks on the Home/Up button, so long
// as you specify a parent activity in AndroidManifest.xml.
        val id: Int = item.getItemId()
        if (id == R.id.action_refresh) {
            if (appUtils.verifyAvailableNetwork(this) == true) {
                mPresenter.fetchMarvelHeroesList()
            } else {
                Toast.makeText(this, resources.getString(R.string.warning_network_error), Toast.LENGTH_LONG).show()
            }
            return true
        }
        return super.onOptionsItemSelected(item)
    }



    override fun onDestroy() {
        super.onDestroy()
        (application as SampleApplication).releaseComponent()
        mPresenter.detachView()
        mPresenter.unRegister()
    }

    override fun onItemClicked(name: String,realName: String, team: String, firstappearance: String, publisher: String, imageurl: String, bio: String) {
        startActivity(Intent(this@HeroesListActivity, HeroesDetailActivity::class.java)
                .putExtra("name", name)
                .putExtra("realName", realName)
                .putExtra("team", team)
                .putExtra("firstappearance", firstappearance)
                .putExtra("publisher", publisher)
                .putExtra("imageurl", imageurl)
                .putExtra("bio", bio)
        )
    }
    }



