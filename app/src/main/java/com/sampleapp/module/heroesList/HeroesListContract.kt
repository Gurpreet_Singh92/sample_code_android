package com.sampleapp.module.heroesList

import com.sampleapp.data.response.HeroesListResponse
import com.sampleapp.mvp.BaseMvpPresenter
import com.sampleapp.mvp.BaseMvpView


object HeroesListContract {

    interface View : BaseMvpView {
        fun heroesList(heroesListResponse: HeroesListResponse)

        fun showProgress()

        fun hideProgress()

    }

    interface Presenter : BaseMvpPresenter<View> {
        fun unRegister()

    }
}