package com.sampleapp.module.heroesList

import com.sampleapp.R
import com.sampleapp.api.RestService
import com.sampleapp.data.response.HeroesListResponse
import com.sampleapp.mvp.BaseMvpPresenterImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


class HeroesListPresenter (private val mRestService: RestService) :
        BaseMvpPresenterImpl<HeroesListContract.View>(), HeroesListContract.Presenter {

    private var disposable: Disposable? = null

    /**
     * Api implementation to fetch Marvel heroes list
     */
    fun fetchMarvelHeroesList() {
        mView?.showProgress()
        disposable = mRestService.heroesListFetching()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    mView?.hideProgress()
                    when {
                        it.size>0->
                            mView?.heroesList(it as HeroesListResponse)
                        else -> mView?.showError(R.string.empty_data)

                    }
                }, {
                    mView?.hideProgress()
                    mView?.showError(it)

                })
    }

    override fun unRegister() {
        disposable?.dispose()
    }
}