package com.sampleapp.module.heroesList

import com.sampleapp.api.RestService

import com.sampleapp.utils.ProgressBarHandler
import dagger.Module
import dagger.Provides



@Module
class HeroesListModule(private var heroesListActivity: HeroesListActivity) {

    @Provides
    fun getPresenter(restService: RestService): HeroesListPresenter = HeroesListPresenter(restService)

    @Provides
    fun getProgressBar(): ProgressBarHandler {
        return ProgressBarHandler(heroesListActivity)
    }

}