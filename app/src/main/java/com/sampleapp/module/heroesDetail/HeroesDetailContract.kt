package com.sampleapp.module.heroesDetail

import com.sampleapp.mvp.BaseMvpPresenter
import com.sampleapp.mvp.BaseMvpView


object HeroesDetailContract {

    interface View : BaseMvpView {

        fun showProgress()

        fun hideProgress()

    }

    interface Presenter : BaseMvpPresenter<View> {
        fun unRegister()

    }
}