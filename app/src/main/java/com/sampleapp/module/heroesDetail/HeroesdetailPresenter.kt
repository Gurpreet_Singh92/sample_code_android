package com.sampleapp.module.heroesDetail

import com.sampleapp.api.RestService
import com.sampleapp.mvp.BaseMvpPresenterImpl
import io.reactivex.disposables.Disposable

class HeroesdetailPresenter(private val mRestService: RestService) :
        BaseMvpPresenterImpl<HeroesDetailContract.View>(), HeroesDetailContract.Presenter {

    private var disposable: Disposable? = null


    override fun unRegister() {
        disposable?.dispose()
    }
}