package com.sampleapp.module.heroesDetail

import javax.inject.Scope


@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class HeroesDetailScope