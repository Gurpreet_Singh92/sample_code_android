package com.sampleapp.module.heroesDetail

import dagger.Subcomponent



@HeroesDetailScope
@Subcomponent(modules = [(HeroesDetailModule::class)])
interface HeroesDetailComponent {
    fun inject(heroesDetailActivity: HeroesDetailActivity)
}