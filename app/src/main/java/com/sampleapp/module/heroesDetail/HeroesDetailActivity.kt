package com.sampleapp.module.heroesDetail

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.sampleapp.R

import com.sampleapp.module.application.SampleApplication
import com.sampleapp.mvp.BaseMvpActivity
import com.sampleapp.utils.AppUtils
import com.sampleapp.utils.ProgressBarHandler
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_hero_detail.*
import javax.inject.Inject


class HeroesDetailActivity : BaseMvpActivity(), HeroesDetailContract.View {

    lateinit var name:String
    lateinit var realName:String
    lateinit var team:String
    lateinit var firstappearance:String
    lateinit var publisher:String
    lateinit var imageurl:String
    lateinit var bio:String
    @Inject
    lateinit var mPresenter: HeroesdetailPresenter
    @Inject
    lateinit var mProgressBarHandler: ProgressBarHandler

    var list= ArrayList<Any>()

    lateinit var mLayoutManager: androidx.recyclerview.widget.LinearLayoutManager

    @Inject
    lateinit var appUtils: AppUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hero_detail)
        (application as SampleApplication).createHeroDetailComponent(this)?.inject(this)

        mPresenter.attachView(this)

        myToolbar.setNavigationIcon(R.mipmap.back_arrow);
        myToolbar.setNavigationOnClickListener(View.OnClickListener {
            finish()
        })
        mLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)

        name = intent.getStringExtra("name")
        realName = intent.getStringExtra("realName")
        team = intent.getStringExtra("team")
        firstappearance = intent.getStringExtra("firstappearance")
        publisher = intent.getStringExtra("publisher")
        imageurl = intent.getStringExtra("imageurl")
        bio = intent.getStringExtra("bio")

        myToolbar.setNavigationIcon(R.mipmap.back_arrow);
        myToolbar.setTitle(name)
        myToolbar.setNavigationOnClickListener(View.OnClickListener {
            finish()
        })

setDetails()
        if (appUtils.verifyAvailableNetwork(this) == true) {
          //  mPresenter.fetchClosingItemDetail(listingId)
            setImage_Url()
        } else {
            Toast.makeText(this, resources.getString(R.string.warning_network_error), Toast.LENGTH_LONG).show()
        }

    }

    private fun setDetails() {
        real_name.text="Real Name: "+realName
        team_name.text="Team: "+team
        tv_firstappearance.text="First appearance: "+firstappearance
        tv_publisher.text="Publisher: "+publisher
        tv_bio.text="Bio: "+bio
    }

    private fun setImage_Url() {
        Picasso.get().load(imageurl)
                .placeholder(R.mipmap.placeholder_img)
                .into(hero_img);
    }


    override fun showProgress() {
        mProgressBarHandler.showProgress()
    }

    override fun hideProgress() {
        mProgressBarHandler.hideProgress()
    }

    /**
     * release all listeners and components
     */
    override fun onDestroy() {
        super.onDestroy()
        mPresenter.detachView()
        mPresenter.unRegister()
    }
}


