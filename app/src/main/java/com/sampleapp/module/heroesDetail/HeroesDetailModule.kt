package com.sampleapp.module.heroesDetail

import com.sampleapp.api.RestService

import com.sampleapp.utils.ProgressBarHandler
import dagger.Module
import dagger.Provides


@Module
class HeroesDetailModule(private var heroesDetailActivity: HeroesDetailActivity) {
    @Provides
    fun getPresenter(restService: RestService): HeroesdetailPresenter = HeroesdetailPresenter(restService)

    @Provides
    fun getProgressBar(): ProgressBarHandler {
        return ProgressBarHandler(heroesDetailActivity)
    }
}