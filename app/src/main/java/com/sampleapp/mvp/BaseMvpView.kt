package com.sampleapp.mvp

import androidx.annotation.StringRes


interface BaseMvpView {

    fun showError(throwable: Throwable)

    fun showError(@StringRes stringResId: Int)

    fun showError(error: String)

    fun showMessage(@StringRes srtResId: Int)

    fun showMessage(message: String)

}
