package com.sampleapp.data.response

class HeroesListResponse : ArrayList<HeroesListResponseItem>()

data class HeroesListResponseItem(
    val bio: String,
    val createdby: String,
    val firstappearance: String,
    val imageurl: String,
    val name: String,
    val publisher: String,
    val realname: String,
    val team: String
)