package com.sampleapp.api

import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class NetModule {

    @Provides
    @Singleton
    fun getRestService() = ApiManager.create()
}