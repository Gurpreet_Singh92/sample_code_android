package com.sampleapp.api

import com.sampleapp.constants.ApiConstants
import com.sampleapp.data.response.HeroesListResponse

import io.reactivex.Observable
import retrofit2.http.*

interface RestService {

    //heroes list API
    @GET(ApiConstants.HEROES_LIST)
    fun heroesListFetching(): Observable<HeroesListResponse>

}